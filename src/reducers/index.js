import {FILTERBYGENRE, SEARCHING, SETINITDATA, APDATINGDATA} from "../actions/types";
import array from "../db.json";

let initialArray = [...array]
const initialState = {
    cardList: [...initialArray]
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FILTERBYGENRE:
            const filteredGenre =initialArray.filter(n => !action.payload || n.genre === action.payload);
            return {
                ...state,
                cardList: filteredGenre
            };
        case APDATINGDATA: 
            initialArray.push(action.payload)
            return {
                ...state,
                cardList: [...initialArray]
            };
        case SETINITDATA: 
            return {
                ...initialArray,
            };
        case SEARCHING:
            return {
                ...state,
                cardList: action.payload
            };
        default:
            return state;
    }
}

export default reducer;