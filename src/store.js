import {createStore} from "redux";
import reducer from "./reducers";
import { combineReducers } from "redux";

const allReducers = combineReducers({
    reducer
   });

const store = createStore(allReducers)

export default store;
