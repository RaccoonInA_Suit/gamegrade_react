import React, {useState, useEffect} from "react";
import GameCard from "../GameCard/GameCard";
import GenreFilter from "../GenreFilter/GenreFilter";
import SearchBar from "../SearchBar/SearchBar";
import FormDialog from "../FormButton/FormButton";
import {useDispatch, useSelector} from 'react-redux';
import {SEARCHING, FILTERBYGENRE, SETINITDATA,APDATINGDATA } from "../../actions/types";
import Fuse from "fuse.js";
import Aos from "aos";
import'aos/dist/aos.css';

import "./Content.scss";


const Content = () => {

    const listFromReducer = useSelector(state => state.reducer?.cardList)

    const dispatch = useDispatch()

    const [filter, setFilter ] = useState(" ");

    useEffect(() => {
        Aos.init({ duration: 2000});
    }, []);

    const searchData = (pattern) => {
        if (!pattern) {
            dispatch({type: SETINITDATA})
            return;
        }

        const fuse = new Fuse(listFromReducer, {
            keys: ["name"],
        });

        const result = fuse.search(pattern);
        const matches = [];
        if (!result.length) {
            dispatch({type: SETINITDATA});
        } else {
            result.forEach(({item}) => {
                matches.push(item);
            });
            dispatch({type: SEARCHING, payload: matches})
        }
    };

    const genres = React.useMemo(() => [...new Set(listFromReducer.map(n => n.genre))], [ listFromReducer.cardList ]);

    const onGenreChange = (e) => {
        setFilter(e.target.value)
            dispatch({type: FILTERBYGENRE, payload : e.target.value})
    } 

    const formInfoChange = (obj) => {
        dispatch({type: APDATINGDATA, payload: obj});
    };


    return (
        <div>
            <div className="filter-wrapper">
                <GenreFilter value={filter} onChange={onGenreChange} genres={genres} /> 
                <SearchBar onChange={(e) => searchData(e.target.value)}/>
            </div>
            <div className="counter">Completed games: {listFromReducer.length}</div>
            <div className="youtube-wrapper">
                <iframe width="410" height="230" src="https://www.youtube.com/embed/nabIjrbBYyg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <iframe width="410" height="230" src="https://www.youtube.com/embed/mJpehGauU5c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <iframe width="410" height="230" src="https://www.youtube.com/embed/goGh66mcT6g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <FormDialog className="form-dialog" onSuccess={formInfoChange}/>
            <div className="py-4 py-lg-5 container">
                <div data-aos="fade-up" className="row">
                    {listFromReducer.map((Info) => 
                    <GameCard data-aos="fade-up" CardInfo={Info}/>
                    )}
                </div>
            </div>
        </div>
    );
}

export default Content