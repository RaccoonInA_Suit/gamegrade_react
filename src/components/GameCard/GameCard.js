import React from "react";
import image from "./allGames.jpg"

import "./GameCard.scss";


const GameCard = ({CardInfo, filteredGenre}) => {

    return (
    <div className="coll-11 col-md-6 col-lg-3 mx-0 mx-4 margin">
        <div onChange= {() => filteredGenre} key={CardInfo.id} class="card p-0 overflow-hidden h-100 shadow">
            <img src = {image}  class="card-img-top" alt="..."/>
            <div class="card-body">
                <h1>{CardInfo.name}</h1>
                <p class="card-genre">Genre: {CardInfo.genre}</p>
                <p class="card-grade">Rating: {CardInfo.rating}</p>
            </div>
        </div>
    </div>
    );
}

export default GameCard