import React from "react";

import "./GenreFilter.scss";


const GenreFilter = ({ value, onChange, genres }) => {

  return (
    <div>
      <select className="option-header" onChange={onChange} value={value}>
        <option value=""> Genre </option>
        {genres.map(n => <option key={n}>{n}</option>)}
      </select>
    </div>
  )
};

 export default GenreFilter;