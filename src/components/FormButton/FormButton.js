import React, {useState} from "react";
import { Button, Modal } from 'react-bootstrap';

import "./FormButton.scss";


const FormDialog = (props) => {
  const [show, setShow] = useState(false);

  const handleClickShow = () => {
    setShow(prevState => !prevState);
  };


  const [current, setCurrent] = useState({name: "",});
  const formInfoChange = (value, key) => {
    setCurrent({...current, [key]: value})
  };

  return (
    <>
      <Button className="form-dialog" variant="primary" onClick={handleClickShow}>
        <div className="plus"> &#43; </div>
      </Button>

      <Modal show={show} onHide={handleClickShow}>
        <Modal.Header closeButton>
          <Modal.Title>Add game</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input id="name" label="name" onChange={(event) =>formInfoChange(event.target.value, "name")} type="text" class="form-control" placeholder="Name of the game" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"/>
          <select id="genre" label="genre" onChange={(event) =>formInfoChange(event.target.value, "genre")}>
            <option>Please, choose a genre</option>
            <option>RPG</option>
            <option>Action/RPG</option>
            <option>Action</option>
            <option>Strategy</option>
            <option>JRPG</option>
          </select>
          {/* <input id="genre" label="genre" onChange={(event) =>formInfoChange(event.target.value, "genre")} type="text" class="input-genre" placeholder="Genre"/> */}
          <select id="rating" label="rating" onChange={(event) =>formInfoChange(event.target.value, "rating")}>
            <option>Please, rate the game</option>
            <option>0</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
          {/* <input id="rating" label="rating" onChange={(event) =>formInfoChange(event.target.value, "rating")} type="text" class="input-grade" placeholder="Rating" title="Rate 0-5"/> */}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClickShow}>
            Close
          </Button>
          <Button variant="primary" onClick={()=>props.onSuccess(current, handleClickShow(false))}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default FormDialog;