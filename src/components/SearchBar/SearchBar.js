import React from "react";

import "./SearchBar.scss";


const SearchBar = (props) => {
    return (
        <div class="input-group input-group-lg">
            <span class="input-group-text" id="inputGroup-sizing-lg">Searche the game</span>
            <input type="text" onChange={props.onChange} class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg"/>
        </div>	
    );
}

export default SearchBar;